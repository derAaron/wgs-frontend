import { WgsFrontendPage } from './app.po';

describe('wgs-frontend App', () => {
  let page: WgsFrontendPage;

  beforeEach(() => {
    page = new WgsFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
