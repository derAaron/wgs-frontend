import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileBaseComponent } from './tile-base.component';

describe('TileBaseComponent', () => {
  let component: TileBaseComponent;
  let fixture: ComponentFixture<TileBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
