import { OnInit, Input } from "@angular/core";
import { WeatherService } from "../weather.service";
import { Broadcaster } from "ng2-cable/ts";
import { capitalize } from "lodash";

export class TileBaseComponent implements OnInit {
  @Input() name = "";
  model;

  constructor( protected type: string,
               protected weatherService: WeatherService,
               protected broadcaster: Broadcaster ) {
  }

  ngOnInit() {
    this.weatherService.getResource(this.type.toLowerCase(), this.name).subscribe(
      ( model )=> {
        this.model = model;
        this.broadcaster.on<any>(`Input${capitalize(this.type)}.${model.name}`).subscribe(
          data => this.model = data
        );
      });
  }
}
