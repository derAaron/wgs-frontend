import { Component, OnInit } from '@angular/core';
import { PageComponent } from "../page/page.component";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent extends PageComponent {
  constructor() {
    super();
  }
}
