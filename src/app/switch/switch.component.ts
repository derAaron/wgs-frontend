import {Component, EventEmitter, OnInit, Input, Output } from "@angular/core";
import {SwitchService} from "../switch.service";
import {MdDialog} from "@angular/material";
import {SwitchDialogComponent} from "../switch-dialog/switch-dialog.component";
import { Broadcaster } from "ng2-cable/ts";

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.css']
})
export class SwitchComponent implements OnInit {

  @Input() switch:any;

  constructor(private switchService:SwitchService,
              public dialog:MdDialog,
              private broadcaster: Broadcaster) {
  }

  ngOnInit() {
    this.broadcaster.on<any>(`OutputTrigger.${this.switch.output.name}`).subscribe(
      data => this.switch.output = data);
    this.broadcaster.on<any>(`InputSwitch.${this.switch.input.name}`).subscribe(
      data => this.switch.input = data);
  }

  private showDialog = () => {
    let dialogRef = this.dialog.open(SwitchDialogComponent, {
      data: this
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) this.updateSwitch(result);
    });
  }

  private updateSwitch = (state: any) => {
    this.switchService.updateSwitch(this.switch.input.name, state)
      .subscribe(null, error =>  console.error(error));
  }
}
