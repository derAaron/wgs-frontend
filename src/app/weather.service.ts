import { Injectable } from "@angular/core";
import { environment } from "../environments/environment";
import { Observable } from "rxjs/Observable";
import { Http, Response } from "@angular/http";
import { Globals } from "./globals";

@Injectable()
export class WeatherService {
  constructor( private http:Http,
               private globals:Globals ) {
  }

  getResource( resource:string, name:string ):Observable<any> {
    this.globals.openConnections++;
    return this.http.get(`${environment.baseUrl}/${resource}/${name}`)
      .map(( res:Response ) => {
        let body = res.json();
        return body || {};
      })
      .catch(this.handleError)
      .finally(() => this.globals.openConnections = Math.max(this.globals.openConnections - 1, 0));
  }

  private handleError( error:Response | any ) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg:string;
    if ( error instanceof Response ) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
