import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconValueComponent } from './icon-value.component';

describe('IconValueComponent', () => {
  let component: IconValueComponent;
  let fixture: ComponentFixture<IconValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
