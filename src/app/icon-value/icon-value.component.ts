import { Component, Input } from "@angular/core";
import { isNumeric } from "rxjs/util/isNumeric";

@Component({
  selector: 'app-icon-value',
  templateUrl: './icon-value.component.html',
  styleUrls: [ './icon-value.component.css' ]
})
export class IconValueComponent {
  @Input() icon:string = "default";
  @Input() value:string = "n/a";
  @Input() unit:string = "";

  public isNumber() {
    return isNumeric(this.value) || /^[0-9.]+$/.test(this.value);
  }
}
