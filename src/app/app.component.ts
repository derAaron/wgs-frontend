import { Component } from "@angular/core";
import { Ng2Cable, Broadcaster } from "ng2-cable/ts";
import { environment } from "./../environments/environment";
import { Globals } from "./globals";
import { MdSnackBar } from "@angular/material";

const SCREENSAVER_TIMEOUT = 60 * 1000; //in ms

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent {
  page = 'weather';
  public lastInteraction: number = 0;

  private navigateTo = ( page ) => {
    this.page = page;
  }

  constructor( private ng2cable:Ng2Cable,
               private broadcaster:Broadcaster,
               private globals:Globals,
               private snackBar:MdSnackBar ) {
    this.ng2cable.subscribe(`${environment.baseUrl}/cable`, 'UpdateChannel'); // for development need to change url to http://localhost:3000/cable

    // Show Errors in the Snackbar and Console
    this.broadcaster.on<any>(`Error`).subscribe((error) => {
      this.snackBar.open(error.snackbar, "OK");
      console.info(error.console);
    });

    this.interact();
  }

  public interact(){
    this.lastInteraction = new Date().getTime();
  }

  public hideScreensaver(): boolean {
    return (this.lastInteraction + SCREENSAVER_TIMEOUT) > new Date().getTime();
  }

  public hideSpinner(): boolean{
    return this.globals.openConnections === 0;
  }
}
