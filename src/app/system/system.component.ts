import { Component, OnInit, OnDestroy } from "@angular/core";
import { PageComponent } from "../page/page.component";
import { SystemService } from "../system.service";
import { MdDialog } from '@angular/material';
import { PowerDialogComponent } from "../power-dialog/power-dialog.component";
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: [ './system.component.css' ]
})
export class SystemComponent extends PageComponent implements OnInit, OnDestroy {
  system:any;
  private getAll$: any;

  constructor( private systemService:SystemService,
               public dialog:MdDialog ) {
    super();
  }

  ngOnInit() {
    this.loadSystem();
  }

  ngOnDestroy() {
    console.log(Date.parse("Fri, 11 Aug 2017 20:21:02 UTC +00:00"));
    this.getAll$.unsubscribe();
  }

  public openPowerDialog() {
    let dialogRef = this.dialog.open(PowerDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      switch ( result ) {
        case 'shutdown':
          this.systemService.shutdown();
          break;
        case 'wgsrestart':
          this.systemService.wgsrestart();
          break;
        case 'reload':
          window.location.reload(true);
      }
    });
  }

  private loadSystem() {
    this.getAll$ = this.systemService.getAll().subscribe(
      ( data )=>this.system = data);
  }
}
