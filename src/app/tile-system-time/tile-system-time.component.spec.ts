import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileSystemTimeComponent } from './tile-system-time.component';

describe('TileSystemTimeComponent', () => {
  let component: TileSystemTimeComponent;
  let fixture: ComponentFixture<TileSystemTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileSystemTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileSystemTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
