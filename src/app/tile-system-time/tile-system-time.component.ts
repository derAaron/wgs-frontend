import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tile-system-time',
  templateUrl: './tile-system-time.component.html',
  styleUrls: ['./tile-system-time.component.css']
})
export class TileSystemTimeComponent {
  @Input()
  public header;
  @Input()
  public time;
}
