import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import {
  MdButtonModule,
  MdDialogModule,
  MdCardModule,
  MdGridListModule,
  MdProgressSpinnerModule,
  MdSnackBarModule
} from "@angular/material";
import { HttpModule, JsonpModule } from "@angular/http";
import { AppComponent } from "./app.component";
import { ButtonsComponent } from "./buttons/buttons.component";
import { SwitchComponent } from "./switch/switch.component";
import { SwitchService } from "./switch.service";
import { SwitchDialogComponent } from "./switch-dialog/switch-dialog.component";
import { WeatherComponent } from "./weather/weather.component";
import { TachoComponent } from "./tacho/tacho.component";
import { Ng2CableModule } from "ng2-cable/ts";
import { TileSensorComponent } from "./tile-sensor/tile-sensor.component";
import { TileWeatherstationComponent } from "./tile-weatherstation/tile-weatherstation.component";
import { TileSolarpanelComponent } from "./tile-solarpanel/tile-solarpanel.component";
import { TileWindowComponent } from "./tile-window/tile-window.component";
import { WeatherService } from "./weather.service";
import { IconValueComponent } from "./icon-value/icon-value.component";
import { Globals } from "./globals";
import { ScreensaverComponent } from './screensaver/screensaver.component';
import { LongPressDirective } from './long-press.directive';
import { ShortPressDirective } from './short-press.directive';
import { SystemComponent } from './system/system.component';
import { SystemService } from "./system.service";
import { TileSystemMonitoringComponent } from './tile-system-monitoring/tile-system-monitoring.component';
import { TileNetworkMonitoringComponent } from './tile-network-monitoring/tile-network-monitoring.component';
import { PowerDialogComponent } from './power-dialog/power-dialog.component';
import { TileSystemTimeComponent } from './tile-system-time/tile-system-time.component';

@NgModule({
  declarations: [
    AppComponent,
    ButtonsComponent,
    SwitchComponent,
    SwitchDialogComponent,
    WeatherComponent,
    TachoComponent,
    TileSensorComponent,
    TileWeatherstationComponent,
    TileSolarpanelComponent,
    TileWindowComponent,
    IconValueComponent,
    ScreensaverComponent,
    LongPressDirective,
    ShortPressDirective,
    SystemComponent,
    TileSystemMonitoringComponent,
    TileNetworkMonitoringComponent,
    PowerDialogComponent,
    TileSystemTimeComponent,
  ],
  imports: [
    BrowserModule,
    MdGridListModule,
    MdButtonModule,
    MdDialogModule,
    MdCardModule,
    MdProgressSpinnerModule,
    HttpModule,
    JsonpModule,
    NoopAnimationsModule,
    Ng2CableModule,
    MdSnackBarModule
  ],
  providers: [
    SwitchService,
    WeatherService,
    SystemService,
    Globals,
  ],
  bootstrap: [ AppComponent ],
  entryComponents: [
    SwitchDialogComponent,
    PowerDialogComponent
  ]
})
export class AppModule { }
