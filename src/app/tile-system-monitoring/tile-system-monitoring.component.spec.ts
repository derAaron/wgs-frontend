import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileSystemMonitoringComponent } from './tile-system-monitoring.component';

describe('TileSystemMonitoringComponent', () => {
  let component: TileSystemMonitoringComponent;
  let fixture: ComponentFixture<TileSystemMonitoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileSystemMonitoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileSystemMonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
