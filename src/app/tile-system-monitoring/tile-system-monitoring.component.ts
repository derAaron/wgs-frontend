import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tile-system-monitoring',
  templateUrl: './tile-system-monitoring.component.html',
  styleUrls: ['./tile-system-monitoring.component.css']
})
export class TileSystemMonitoringComponent {
  @Input()
  public header;
  @Input()
  public max;
  @Input()
  public value;
  @Input()
  public redToGreen: boolean;

  public getPercent() {
    return Math.round(100 * this.value / this.max);
  }
}
