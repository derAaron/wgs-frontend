import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerDialogComponent } from './power-dialog.component';

describe('PowerDialogComponent', () => {
  let component: PowerDialogComponent;
  let fixture: ComponentFixture<PowerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
