import { Component, OnInit } from '@angular/core';
import { TileBaseComponent } from "../tile-base/tile-base.component";
import { WeatherService } from "../weather.service";
import { Broadcaster } from "ng2-cable/ts";

@Component({
  selector: 'app-tile-window',
  templateUrl: './tile-window.component.html',
  styleUrls: ['./tile-window.component.css']
})
export class TileWindowComponent extends TileBaseComponent implements OnInit {
  constructor( weatherService: WeatherService,
               broadcaster: Broadcaster ) {
    super("Window", weatherService, broadcaster);
  }

  ngOnInit() {
    this.weatherService.getResource(this.type.toLowerCase(), this.name).subscribe(
      ( model )=> {
        this.model = model;
        this.model.forEach((window, index, windows)=>{
          this.broadcaster.on<any>(`InputWindow.${window.name}`).subscribe(
            data => windows[index] = data
          );
        });
      });
  }
}
