import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileWindowComponent } from './tile-window.component';

describe('TileWindowComponent', () => {
  let component: TileWindowComponent;
  let fixture: ComponentFixture<TileWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
