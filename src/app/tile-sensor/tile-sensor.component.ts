import { Component } from "@angular/core";
import { TileBaseComponent } from "../tile-base/tile-base.component";
import { WeatherService } from "../weather.service";
import { Broadcaster } from "ng2-cable/ts";

@Component({
  selector: 'app-tile-sensor',
  templateUrl: './tile-sensor.component.html',
  styleUrls: [ './tile-sensor.component.css' ]
})
export class TileSensorComponent extends TileBaseComponent {
  constructor( weatherService: WeatherService,
               broadcaster: Broadcaster ) {
    super("Sensor", weatherService, broadcaster);
  }
}
