import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileSensorComponent } from './tile-sensor.component';

describe('TileSensorComponent', () => {
  let component: TileSensorComponent;
  let fixture: ComponentFixture<TileSensorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileSensorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
