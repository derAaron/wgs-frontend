import { Directive, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({ selector: '[shortPress]' })

export class ShortPressDirective {

  @Output()
  shortPress = new EventEmitter();

  private _timeout: any;
  private _isShort: boolean;

  @HostListener('mousedown') onMouseDown( e ) {
    this._isShort = true;
    this._timeout = setTimeout(() => {
      this._isShort = false;
    }, 500);
  }

  @HostListener('mouseup') onMouseLeave( e ) {
    if (this._isShort) {
      this.shortPress.emit( e );
    }
    clearTimeout(this._timeout);
  }

  @HostListener('mouseleave') onMouseUp() {
    clearTimeout(this._timeout);
  }
}