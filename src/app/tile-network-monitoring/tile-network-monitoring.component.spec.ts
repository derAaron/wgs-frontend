import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileNetworkMonitoringComponent } from './tile-network-monitoring.component';

describe('TileNetworkMonitoringComponent', () => {
  let component: TileNetworkMonitoringComponent;
  let fixture: ComponentFixture<TileNetworkMonitoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileNetworkMonitoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileNetworkMonitoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
