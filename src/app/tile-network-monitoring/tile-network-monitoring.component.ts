import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tile-network-monitoring',
  templateUrl: './tile-network-monitoring.component.html',
  styleUrls: ['./tile-network-monitoring.component.css']
})
export class TileNetworkMonitoringComponent {
  @Input()
  public header: string;
  @Input()
  public ipLocal: string;
  @Input()
  public ipRemote: string;
  @Input()
  public ssid: string;
}
