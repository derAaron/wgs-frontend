import { Component, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'app-tacho',
  templateUrl: './tacho.component.html',
  styleUrls: [ './tacho.component.css' ]
})
export class TachoComponent implements OnChanges {
  @Input() max: number;
  @Input() value: number;
  @Input() redToGreen: boolean;
  @Input() thickness: number = 24;
  @Input() width: number = 128;
  @Input() borderWidth: number = 4;

  radius: number;
  borderPath: string;
  fillPath: string;
  height: number;
  fillColor: string;

  constructor() {
  }

  ngOnChanges() {
    this.radius = (this.width - this.borderWidth) / 2;
    this.height = this.radius + this.borderWidth;
    const yMax = this.height - (this.borderWidth / 2);
    const xMax = this.width - (this.borderWidth / 2);
    this.fillPath = `M ${ this.borderWidth / 2 },${ yMax }
  A ${ this.radius } ${ this.radius } 0 0 1 ${ this.getXY(this.radius) }
  L ${ this.getXY(this.radius - this.thickness) }
  A ${ this.radius - this.thickness } ${ this.radius - this.thickness} 0 0 0 ${ 2 + this.thickness }, ${ yMax }
  Z`;
    this.borderPath = `M ${ this.borderWidth / 2 }, ${ yMax }
  A ${ this.radius } ${ this.radius } 0 1 1 ${ this.borderWidth / 2 + 2 * this.radius }, ${ yMax }
  L ${ 2 * this.radius + this.borderWidth / 2 - this.thickness },${ yMax }
  A ${ this.radius - this.thickness } ${ this.radius - this.thickness} 0 1 0 ${ this.borderWidth / 2 + this.thickness }, ${ yMax}
  Z`;
    const per120s = (120 * this.value / this.max);
    this.fillColor = `hsl(${this.redToGreen ? per120s : 120 - per120s}, 100%, 50%)`;
  }

  private getXY( radius:number ):string {
    const percentage = this.value / this.max;
    const angle = percentage * Math.PI;
    const centerX = this.radius + this.borderWidth / 2;
    const centerY = this.height;
    const x = Math.cos(angle) * radius;
    const y = Math.sin(angle) * radius;
    return `${ centerX - x } ${ centerY - y }`;
  }
}
