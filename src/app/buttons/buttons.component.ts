import { Component, OnInit } from "@angular/core";
import { SwitchService } from "../switch.service";
import { PageComponent } from "../page/page.component";
import { Ng2Cable, Broadcaster } from "ng2-cable/ts";
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: [ './buttons.component.css' ]
})
export class ButtonsComponent extends PageComponent implements OnInit {
  loaded: boolean = false;
  switches: any[];

  constructor( private switchService:SwitchService,
               private ng2cable:Ng2Cable,
               private broadcaster:Broadcaster ) {
    super();
  }

  ngOnInit() {
    this.loadSwitches();
  }

  private loadSwitches = () => {
    this.switchService.getAll().subscribe(
      (switches)=>this.switches = switches);
  }
}
