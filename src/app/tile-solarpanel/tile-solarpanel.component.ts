import { Component } from "@angular/core";
import { TileBaseComponent } from "../tile-base/tile-base.component";
import { WeatherService } from "../weather.service";
import { Broadcaster } from "ng2-cable/ts";

@Component({
  selector: 'app-tile-solarpanel',
  templateUrl: './tile-solarpanel.component.html',
  styleUrls: ['./tile-solarpanel.component.css']
})
export class TileSolarpanelComponent extends TileBaseComponent {
  constructor( weatherService: WeatherService,
               broadcaster: Broadcaster ) {
    super("Solarpanel", weatherService, broadcaster);
  }
}
