import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileSolarpanelComponent } from './tile-solarpanel.component';

describe('TileSolarpanelComponent', () => {
  let component: TileSolarpanelComponent;
  let fixture: ComponentFixture<TileSolarpanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileSolarpanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileSolarpanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
