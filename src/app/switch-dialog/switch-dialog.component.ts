import { Component, Inject } from "@angular/core";
import { MD_DIALOG_DATA } from "@angular/material";

@Component({
  selector: 'app-switch-dialog',
  templateUrl: './switch-dialog.component.html',
  styleUrls: ['./switch-dialog.component.css']
})
export class SwitchDialogComponent {

  constructor(@Inject(MD_DIALOG_DATA) public data: any) { }
}
