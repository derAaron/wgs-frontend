import { Component } from "@angular/core";
import { WeatherService } from "../weather.service";
import { Broadcaster } from "ng2-cable/ts";
import { TileBaseComponent } from "../tile-base/tile-base.component";

@Component({
  selector: 'app-tile-weatherstation',
  templateUrl: './tile-weatherstation.component.html',
  styleUrls: ['./tile-weatherstation.component.css']
})
export class TileWeatherstationComponent extends TileBaseComponent {
  constructor( weatherService: WeatherService,
               broadcaster: Broadcaster ) {
    super("Weatherstation", weatherService, broadcaster);
  }
}
