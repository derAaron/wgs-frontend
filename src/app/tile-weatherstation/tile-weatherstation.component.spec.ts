import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TileWeatherstationComponent } from './tile-weatherstation.component';

describe('TileWeatherstationComponent', () => {
  let component: TileWeatherstationComponent;
  let fixture: ComponentFixture<TileWeatherstationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TileWeatherstationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TileWeatherstationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
