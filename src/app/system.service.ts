import { Injectable } from "@angular/core";
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { Globals } from "./globals";
import { Http, Response } from "@angular/http";
import { environment } from "../environments/environment";

@Injectable()
export class SystemService {
  private systemUrl = `${environment.baseUrl}/system`;  // URL to web API

  constructor( private http:Http,
               private globals:Globals ) {
  }

  getAll():Observable<any> {
    this.globals.openConnections++;
    return Observable.interval(1000)
      .switchMap(() => this.http.get(`${this.systemUrl}/`)
        .map(( res:Response ) => {
          this.globals.openConnections = Math.max(this.globals.openConnections - 1, 0)
          let body = res.json();
          return body || {};
        })
        .catch(this.handleError));
  }

  shutdown = () => this.http.get(`${this.systemUrl}/shutdown`).subscribe(console.log);

  wgsrestart = () => this.http.get(`${this.systemUrl}/wgsrestart`).subscribe(console.log);

  private handleError( error:Response | any ) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg:string;
    if ( error instanceof Response ) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
