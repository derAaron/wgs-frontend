import { Directive, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({ selector: '[longPress]' })

export class LongPressDirective {

  @Output()
  longPress = new EventEmitter();

  private _timeout: any;

  @HostListener('mousedown') onMouseDown( e ) {
    this._timeout = setTimeout(() => {
      this.longPress.emit( e );
    }, 500);
  }

  @HostListener('mouseup') onMouseLeave() {
    clearTimeout(this._timeout);
  }

  @HostListener('mouseleave') onMouseUp() {
    clearTimeout(this._timeout);
  }
}