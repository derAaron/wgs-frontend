import { Output, EventEmitter } from '@angular/core';

export abstract class PageComponent {

  @Output() onNavigate = new EventEmitter<string>();

  constructor() {}

  navigateTo = (page: string) => {
    this.onNavigate.emit(page);
  }
}
